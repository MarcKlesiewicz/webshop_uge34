﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductRespoitory
{
    public interface IRepository<T>
    {
        public void Create();
        public void Read();
        public void Update();
        public void Delete();
    }
}
