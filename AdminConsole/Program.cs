﻿using System;
using AdminConsole.Models;

namespace AdminConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = true;
            while (exit)
            {
                try
                {
                Console.WriteLine("Velkommen Admin \nTast 1 for at oprette en ny vare \nTast 2 for at se varekatalog \nTast 3 for ændre en vare \nTast 4 for at slette en vare \nTast 0 for at stoppe programmet");
                int UserInput = int.Parse(Console.ReadLine());
                    switch (UserInput)
                    {
                        case 1:
                            Add();
                            break;
                        case 2:
                            Read();
                            break;
                        case 3:
                            Update();
                            break;
                        case 4:
                            Delete();
                            break;
                        case 0:
                            exit = false;
                            break;

                        default:
                            Console.WriteLine("Ugyldigt input, forsøg igen");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Ugyldigt input, prøv igen");
                }

            }
        }
            
        public static void Add()
        {
            Console.WriteLine("Indtast produktets navn");
            string ProductTitle = Console.ReadLine();
            Console.WriteLine("Indtast produktets pris");
            double ProductPrice = 0;
            while (ProductPrice == 0)
            {
                try
                {
                    ProductPrice = double.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ugyldigt input, prøv igen");
                }
            }
            Console.WriteLine("Indtast produktets farve");
            string ProductColor = Console.ReadLine();
            Console.WriteLine("Indtast produktets beskrivelse");
            string ProductDescription = Console.ReadLine();
            Console.WriteLine("Indtast produktets artikelnummer");
            int ProductID = int.Parse(Console.ReadLine());

            Console.WriteLine($"Følgende produkt er nu oprettet: \nNavn: {ProductTitle}\nPris: {ProductPrice}\nFarve: {ProductColor}\nBeskrivelse: {ProductDescription}\nArtikelnummer: {ProductID}");

            Product myProduct = new Product();
            myProduct.Name = ProductTitle;
            myProduct.Price = ProductPrice;
            myProduct.Color = ProductColor;
            myProduct.Description = ProductDescription;
            myProduct.ArticleNumber = ProductID;

        }
        public static void Read()
        {
            Console.WriteLine("Der er følgende produkter i kataloget:");
        }
        public static void Update()
        {
            Console.WriteLine("Indtast produktets artikelnummer");
            string ProductID = Console.ReadLine();
            //hent artiklen

            Console.WriteLine("Indtast produktets navn");
            string ProductTitle = Console.ReadLine();
            Console.WriteLine("Indtast produktets pris");
            double ProductPrice = double.Parse(Console.ReadLine());
            Console.WriteLine("Indtast produktets farve");
            string ProductColor = Console.ReadLine();
            Console.WriteLine("Indtast produktets beskrivelse");
            string ProductDescription = Console.ReadLine();

        }
        public static void Delete()
        {
            Console.WriteLine("Indtast produktets artikelnummer");
            string ProductID = Console.ReadLine();
            //hent artiklen
            //slet artiklen
        }
    }
}
