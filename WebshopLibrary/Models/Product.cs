﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminConsole.Models
{
    public class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public int ArticleNumber { get; set; }

    }
}
