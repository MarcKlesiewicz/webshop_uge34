﻿using AdminConsole.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AdminConsole.Repo
{
    public class AdminRepository
    {
        private List<Product> products;
        private string fileName;

        public void Add(Product product)
        {
            products.Add(product);
            SaveToFile();
        }

        //public void SaveToFile()

        public Product Read(int id)
        {
            foreach (Product item in products)
            {
                if (id == item.ArticleNumber)
                    return item;
            }
            return null;
        }

        public void Update(int id, Product newProduct)
        {
            Product uproduct = Read(id); // Use the product articlenumber, to find the old product to be replaced.
            if (uproduct != null) // If no product was found, matching the id, do nothing.
            {
                int tempIndex = products.IndexOf(uproduct); // Find index of the product.
                products[tempIndex] = uproduct; // Replace the product in our list, with our new product (information).
            }
            SaveToFile();
        }

        public void Delete(int id)
        {
            Product temp = Read(id); // Get product by id.
            if (temp != null)
                products.Remove(temp); // Remove product from list.
            SaveToFile();
        }

        private void SaveToFile()
        {
            StreamWriter sw = new StreamWriter(fileName);

            foreach (Product item in products)
            {
                String.Format("{0};{1};{2};{3};{4}", item.Name, item.Price, item.Description, item.ArticleNumber);
            }
            sw.Close();
        }

        private void LoadFile()
        {
            StreamReader sr = new StreamReader(fileName);
            string line;
            string[] temp;
            Product tempProduct;
            do
            {
                line = sr.ReadLine();
                temp = line.Split(';');
                tempProduct = new Product();

                tempProduct.Name = temp[0];
                tempProduct.Price = double.Parse(temp[1]);
                tempProduct.Color = temp[2];
                tempProduct.Description = temp[3];
                tempProduct.ArticleNumber = int.Parse(temp[4]);

                products.Add(tempProduct);
            } while (line != null);
            sr.Close();
        }

        public AdminRepository(string fileName)
        {
            this.fileName = fileName;
            products = new List<Product>();
            LoadFile();
        }
    }
}
